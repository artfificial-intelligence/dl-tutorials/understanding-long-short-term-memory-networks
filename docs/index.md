# 장단기 메모리 네트워크 이해 <sup>[1](#footnote_1)</sup>

> <font size="3">순환 신경망이 순차적 데이터 처리에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [개요](./understanding-long-short-term-memory-networks.md#intro)
1. [순환 신경망이란?](./understanding-long-short-term-memory-networks.md#sec_02)
1. [장기 종속성의 문제](./understanding-long-short-term-memory-networks.md#sec_03)
1. [단기 메모리 네트워크의 작동 방식](./understanding-long-short-term-memory-networks.md#sec_04)
1. [장단기 메모리 네트워크의 어플리케이션](./understanding-long-short-term-memory-networks.md#sec_05)
1. [요약](./understanding-long-short-term-memory-networks.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 11 — Understanding Long Short-Term Memory Networks](https://medium.com/womenintechnology/dl-tutorial-11-understanding-long-short-term-memory-networks-dfcbbfff6e45?sk=df0510a7ac557078ae91c5e5528c3a14)를 편역하였다.
