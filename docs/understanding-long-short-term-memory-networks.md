# 장단기 메모리 네트워크 이해

## <a name="intro"></a> 개요
이 포스팅에서는 장단기 기억 네트워크(long short-term memory networks), 또는 줄여서 LSTM에 대해 설명한다. LSTM은 텍스트, 음성, 또는 비디오 같은 순차적인 데이터를 처리할 수 있는 순환 신경망(RNN)의 한 종류이다. LSTM은 특히 신경망의 출력이 여러 시간 단계 전에 발생했던 입력에 의존하는 장기 의존성 문제를 처리하는 데 유용하다.

이 포스팅을 학습하면 다음을 수행할 수 있다.

- 순환 신경망이란 무엇이며 어떻게 작동하는가
- 장기 의존성의 문제와 그것이 RNN에 미치는 영향 이해
- 단기 메모리 네트워크가 장기 의존성 문제를 해결하는 방법 설명
- Keras 라이브러리를 사용하여 Python에서 간단한 LSTM 네트워크 구현
- 텍스트 분류 작업에 LSTM 네트워크 적용

이 포스팅을 학습하려면 Python과 데이터 분석에 대한 기본적인 이해가 필요하다. 또한 다음과 같은 라이브러리를 설치해야 한다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
```

LSTM에 대해 배울 준비가 되었나요? 시작해 보겠다!

## <a name="sec_02"></a> 순환 신경망이란?
LSTM의 세부 사항으로 들어가기 전에 먼저 순환신경망이 무엇이며 어떻게 작동하는지 이해하도록 하자. 순환 신경망, 즉 RNN은 텍스트, 음성 또는 비디오와 같은 순차적인 데이터를 처리할 수 있는 인공 신경망의 일종이다. 각 입력을 독립적으로 처리하는 피드포워드 신경망과 달리 RNN은 메모리를 가지고 있어 이전 입력의 정보를 저장하고 이를 사용하여 현재 출력에 영향을 줄 수 있다.

RNN이 어떻게 작동하는지 설명하기 위해 텍스트 처리의 간단한 예를 생각해 보자. 이전 단어를 기반으로 문장에서 다음 단어를 예측할 수 있는 신경망을 구축하고자 한다고 가정해 보자. 예를 들어, "The sky is"이라는 문장이 주어지면, 네트워크가 "blue"이라는 단어를 예측하기를 원한다. 어떻게 할 것인가?

한 가지 방법은 고정된 수의 단어를 입력으로 받아 가능한 다음 단어에 대한 확률 분포를 출력하는 피드포워드 신경망을 사용하는 것이다. 예를 들어, 세 단어를 입력으로 받아 어휘의 각 단어에 대한 확률 벡터를 출력하는 네트워크를 사용할 수 있다. 네트워크는 다음과 같이 보일 것이다.

```python
# Define the input layer
input_layer = layers.Input(shape=(3,))

# Define the hidden layer
hidden_layer = layers.Dense(64, activation="relu")(input_layer)
# Define the output layer
output_layer = layers.Dense(vocab_size, activation="softmax")(hidden_layer)
# Define the model
model = keras.Model(inputs=input_layer, outputs=output_layer)
```

이 네트워크는 세 단어의 원 핫 인코딩된 벡터를 입력으로 받아 어휘와 동일한 크기의 확률 벡터를 출력할 것이다. 예를 들어 어휘가 10,000개의 단어를 가진다면 입력 벡터와 출력 벡터는 각각 10,000개의 요소를 가질 것이고, 하나의 요소만 1이고 나머지는 0일 것이다. 네트워크는 입력 단어를 따를 가능성이 높은 단어에 더 높은 확률을 할당하는 방법을 배울 것이다.

그러나 이러한 접근 방식에는 몇 가지 한계가 있다. 첫째, 고정된 수의 단어만 입력으로 처리할 수 있으며, 이는 해당 창 너머의 상황을 포착할 수 없음을 의미한다. 예를 들어, 입력이 "The sky is"인 경우 네트워크는 다음 단어로 "blue" 또는 "clear"를 예측할 수 있지만, "The sky is blue today, but it was cloudy yesterday"이 더 큰 맥락의 일부인지는 알 수 없다. 둘째, 다른 입력에 걸쳐 가중치를 공유하지 않으며, 이는 각 단어 위치에 대해 동일한 특징을 학습해야 함을 의미한다. 예를 들어, 입력이 "The sky is blue" 또는 "The sky was blue"인 경우 네트워크는 동일한 단어임에도 불구하고 두 경우 모두 "sky"에 대해 동일한 피처를 학습해야 한다.

더 좋은 방법은 한 번에 하나의 단어를 입력으로 받아 가능한 다음 단어에 대한 확률 분포를 출력하는 순환 신경망을 사용하는 것이다. 네트워크는 또한 이전 입력에서 얻은 정보를 저장하고 각 입력 후에 업데이트하는 숨겨진 상태를 가질 것이다. 네트워크는 다음과 같이 보일 것이다.

```python
# Define the input layer
input_layer = layers.Input(shape=(1,))

# Define the recurrent layer
recurrent_layer = layers.SimpleRNN(64)(input_layer)
# Define the output layer
output_layer = layers.Dense(vocab_size, activation="softmax")(recurrent_layer)
# Define the model
model = keras.Model(inputs=input_layer, outputs=output_layer)
```

이 네트워크는 한 단어의 원 핫 인코딩된 벡터를 입력으로 받아 어휘와 동일한 크기의 확률 벡터를 출력할 것이다. 네트워크는 또한 이전 입력으로부터 정보를 저장하고 각 입력 후에 업데이트하는 크기 64의 숨겨진 상태 벡터를 가질 것이다. 네트워크는 숨겨진 상태에 기초하여 입력 단어를 따를 가능성이 더 높은 단어에 더 높은 확률을 할당하는 것을 학습한다. 네트워크는 또한 다른 입력에 걸쳐 동일한 가중치를 공유할 것이며, 이는 네트워크가 위치에 관계없이 동일한 단어에 대해 동일한 피처를 학습할 수 있다는 것을 의미한다.

순환 신경망을 사용하는 장점은 가변 길이 입력을 처리할 수 있고 이전 입력에서 문맥을 포착할 수 있다는 것이다. 예를 들어, 입력이 "The sky is"라면 네트워크는 다음 단어로 "blue" 또는 "clear"를 예측할 수 있지만, "The sky is blue today, but it was cloudy yesterday"와 같이 문장이 더 큰 문맥의 일부인지 기억하기 위해 숨겨진 상태를 사용할 수도 있다. 네트워크는 각 입력에 대해 동일한 가중치를 공유하기 때문에 두 경우 모두 "sky"이라는 단어에 대해 동일한 피처를 학습할 수도 있다.

보다시피 순환 신경망은 텍스트, 음성 또는 비디오 같이 순차적인 데이터를 처리하는 강력한 도구이다. 그러나 특히 장기 종속성을 다루는 데 있어서는 몇 가지 단점도 있다. 다음 절에서는 장기 종속성이 무엇이며 RNN에 어떤 영향을 미치는지 살펴볼 것이다.

## <a name="sec_03"></a> 장기 종속성의 문제
이전 절에서는 순환 신경망이 순차적 데이터를 처리하고 이전 입력에서 컨텍스트를 캡처하는 방법을 설명하였다. 그러나 RNN은 완벽하지 않으며 장기 종속성을 학습하는 데 어려움을 겪는다는 큰 단점이 있다. 장기 종속성은 무엇이며 왜 중요한가?

장기 종속성은 신경망의 출력이 여러 단계 전에 발생한 입력에 의존하는 경우를 말한다. 예를 들어, 다음 문장을 생각해 보자.

**"I grew up in France, so I speak fluent _________."**

빈칸을 채울 가능성이 가장 높은 단어는 무엇일까? 아마도 여러분은 "French"라고 추측했을 것이고, 여러분이 옳을 것이다. 하지만 여러분은 그것을 어떻게 알았나요? 여러분은 문장의 첫 부분인 "I grew up in France"의 정보를 사용하여 그 사람이 프랑스어를 말할 것이라고 추론했을 것이다. 이는 장기 의존성을 보여주는 예인데, 왜냐하면 출력("French"라는 단어)은 여러 단어 전에 발생했던 입력("France"라는 단어)에 의존하기 때문이다.

장기 의존성은 자연어에서 흔히 볼 수 있으며, 문장의 의미와 구조를 이해하는 데 필수적이다. 그러나 **그래디언트 소실(vanishing gradient)**이라는 문제 때문에 RNN이 학습하기에도 매우 어렵다.

그래디언트 소실은 역전파 과정에서 오차 함수의 기울기가 매우 작아지거나 0이 되는 경우인데, 이는 네트워크가 가중치를 업데이트하고 오류로부터 학습할 수 없음을 의미한다. 이것은 각 시간 단계에서 기울기(gradient)에 각 연결의 가중치를 곱하기 때문에 발생하며, 가중치가 1보다 작으면 시간이 지남에 따라 기울기는 기하급수적으로 감소할 것이다. 따라서 네트워크는 많은 시간 단계 전에 발생한 입력으로부터 학습하는 데 어려움을 겪는다. 왜냐하면 기울기는 그 입력에 도달하기 전에 사라질 것이기 때문이다.

그래디언트 소실이 RNN에 어떤 영향을 미치는지 설명하기 위해 위의 문장에서 장기 의존성을 학습하려고 하는 간단한 RNN의 예를 살펴보자. 네트워크는 다음과 같이 보일 것이다.

```python
# Define the input layer
input_layer = layers.Input(shape=(1,))

# Define the recurrent layer
recurrent_layer = layers.SimpleRNN(64)(input_layer)
# Define the output layer
output_layer = layers.Dense(vocab_size, activation="softmax")(recurrent_layer)
# Define the model
model = keras.Model(inputs=input_layer, outputs=output_layer)
# Compile the model
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
```

이 네트워크는 한 번에 하나의 단어를 입력으로 받아 가능한 다음 단어에 대한 확률 분포를 출력할 것이다. 네트워크는 또한 이전 입력으로부터 정보를 저장하고 각 입력 후에 업데이트하는 64 크기의 숨겨진 상태 벡터를 가질 것이다. 네트워크는 문장으로부터 마지막 쌍("fluent", "French")까지 ("I", "grew"), ("grew","up"), ("up","in") 단어 쌍에 대해 훈련될 것이다. 네트워크는 예측된 단어와 실제 단어 사이의 교차 엔트로피 손실(cross-entropy loss)을 최소화하고 손실 함수의 그레디언트를 사용하여 가중치를 업데이트하려고 노력할 것이다.

그러나 손실 함수의 기울기는 시간이 지남에 따라 사라지기 때문에 네트워크는 "France"와 "French" 사이의 장기 의존성을 학습하는 데 어려움을 겪을 것이다. 네트워크는 각 시간 단계에서 각 연결의 가중치에 기울기를 곱해야 하며, 가중치가 1보다 작으면 기울기는 기하급수적으로 감소한다. 예를 들어 각 연결의 가중치가 0.9이고 마지막 시간 단계에서 손실 함수의 기울기가 1이라고 가정하자. 이전 시간 단계의 기울기는 0.9이고, 그 이전 시간 단계의 기울기는 0.81이므로 첫 번째 시간 단계의 기울기가 0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000387이 될 때까지 계속한다. 이는 매우 적은 숫자이며 가중치 업데이트에 전혀 영향을 미치지 않을 것이다. 따라서 네트워크는 "France"와 "French" 사이의 장기 의존성을 학습하지 않을 것이다.

보다시피, 그레디언트 소실은 RNN에게 심각한 문제이며, 순차 데이터에서 장기 의존성을 학습하지 못하도록 한다. 이 문제를 어떻게 해결할 수 있을까? 한 가지 방법은 그레디언트 소실 문제를 극복하고 장기 의존성을 학습할 수 있는 다른 종류의 순환 신경망을 사용하는 것이다. 이런 종류의 순환 신경망을 장단기 기억 네트워크, 또는 줄여서 LSTM이라고 한다. 다음 절에서는 LSTM이 어떻게 작동하고 장기 의존성 문제를 어떻게 해결하는지 살펴볼 것이다.

## <a name="sec_04"></a> 단기 메모리 네트워크의 작동 방식
앞 절에서 우리는 그레디언트 소실 문제 때문에 순환 신경망이 장기 의존성을 학습하는 데 어려움을 겪는다는 것을 설명하였다. 이 절에서는 장단기 기억 네트워크, 즉 LSTM이 이 문제를 해결하고 어떻게 순차적 데이터에서 장기 의존성을 학습하는지 살펴본다.

LSTM은 우리가 이전에 보았던 단순한 RNN보다 더 복잡한 구조를 가진 특별한 타입의 순환 신경망이다. LSTM은 크게 세 가지 구성 요소를 가지고 있다. **셀 상태(cell state)**, **입력 게이트(input gate)**와 **출력 게이트(output gate)**이다. 셀 상태는 네트워크의 장기 기억을 저장하는 벡터이다. 입력 게이트는 셀 상태에 어떤 정보를 추가할지를 결정한다. 출력 게이트는 셀 상태에서 어떤 정보를 출력할지를 결정한다. 네트워크에는 셀 상태에서 어떤 정보를 제거할지를 결정하는 **망각 게이트(forget gate)**도 있다.

LSTM이 어떻게 작동하는지 이해하기 위해 문장의 장기 의존성을 학습하려는 LSTM 네트워크의 예를 살펴보자.

**"I grew up in France, so I speak fluent _________."**

네트워크는 다음과 같다.

```python
# Define the input layer
input_layer = layers.Input(shape=(1,))

# Define the LSTM layer
lstm_layer = layers.LSTM(64)(input_layer)
# Define the output layer
output_layer = layers.Dense(vocab_size, activation="softmax")(lstm_layer)
# Define the model
model = keras.Model(inputs=input_layer, outputs=output_layer)
# Compile the model
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
```

이 네트워크는 한 번에 하나의 단어를 입력으로 받아 가능한 다음 단어에 대한 확률 분포를 출력할 것이다. 네트워크는 또한 네트워크의 장기 기억을 저장하는 64 크기의 셀 상태 벡터와 셀 상태 안팎의 정보 흐름을 제어하는 64 크기의 4개의 게이트 벡터를 가질 것이다. 네트워크는 문장에서 마지막 쌍("fluent", "French")까지 ("I", "grew"), ("grew","up"), ("up","in") 단어 쌍에 대해 훈련될 것이다. 네트워크는 예측된 단어와 실제 단어 사이의 교차 엔트로피 손실을 최소화하고 손실 함수의 그레디언트를 사용하여 가중치와 게이트를 업데이트하려고 노력할 것이다.

그러나 단순한 RNN과 달리 LSTM 네트워크는 손실 함수의 그레디언트를 시간을 거슬러 올라갈수록 보존할 수 있기 때문에 "France"와 "French" 사이의 장기 의존성을 학습할 수 있을 것이다. 네트워크는 기울기에 각 연결의 가중치를 곱하지 않고 게이트를 사용하여 셀 상태에서 정보를 선택적으로 추가, 제거 또는 출력함으로써 이를 수행할 수 있다. 예를 들어 네트워크가 "France"라는 단어를 입력으로 간주한다고 가정해보자. 네트워크는 다음과 같은 작업을 수행할 것이다,

- 입력 게이트는 시그모이드 함수를 사용하여 게이트 벡터의 해당 요소에 높은 값을 할당함으로써 "France"에 대한 정보를 셀 상태에 추가하기로 결정할 것이다.
- 망각 게이트는 시그모이드 함수를 사용하여 게이트 벡터의 대응하는 요소에 높은 값을 할당함으로써 셀 상태의 이전 입력으로부터 정보를 유지하기로 결정할 것이다.
- 출력 게이트는 시그모이드 함수를 사용하여 게이트 벡터의 대응하는 요소에 높은 값을 할당함으로써, 셀 상태로부터 숨겨진 상태로 정보를 출력하기로 결정할 것이다.
- 셀 상태는 망각 게이트와 이전 셀 상태의 곱에 입력 게이트와 입력 단어의 곱을 추가함으로써 그 값을 업데이트할 것이다.
- 숨겨진 상태는 출력 게이트와 셀 상태를 곱하여 값을 업데이트한다.

이렇게 하면 네트워크는 "France"에 대한 정보를 셀 상태에 저장하고 나중에 시간 단계에서 출력에 영향을 미치는 데 사용할 것이다. 네트워크는 각 연결의 가중치를 곱하는 것이 아니라 게이트를 사용하여 셀 상태에서 정보를 추가하거나 제거하기 때문에 손실 함수의 기울기도 보존될 것이다. 따라서 네트워크는 기울기 소실 문제를 겪지 않고 "France"와 "French" 사이의 장기 의존성을 학습할 수 있을 것이다.

보다시피 LSTM은 사라지는 그래디언트 소실 문제를 극복하고 순차 데이터에서 장기 종속성을 학습할 수 있는 영리하고 강력한 방법이다. LSTM은 텍스트 생성, 기계 번역, 음성 인식 등 복잡하고 다양한 작업을 처리할 수 있다. 다음 절에서는 LSTM의 응용 사례와 Keras 라이브러리를 이용하여 Python으로 구현하는 방법을 살펴볼 것이다.

## <a name="sec_05"></a> 장단기 메모리 네트워크의 어플리케이션
장단기 메모리 네트워크 또는 LSTM은 텍스트, 음성 또는 비디오 같은 순차적인 데이터를 수반하는 다양한 어플리케이션 분야에서 널리 사용된다. 본 절에서는 LSTM을 다양한 작업과 도메인에 적용할 수 있는 방법과 Keras 라이브러리를 사용하여 Python으로 구현하는 방법에 대한 예를 살펴본다.

LSTM의 가장 일반적인 어플리케이션 중 하나는 텍스트 생성으로, 일부 입력이나 컨텍스트를 기반으로 텍스트를 생성하는 작업이다. 예를 들어, LSTM을 사용하여 이미지에 대한 캡션, 기사에 대한 요약, 노래에 대한 가사 또는 프롬프트에 대한 이야기를 생성할 수 있다. 기본 아이디어는 큰 텍스트 코퍼스에 LSTM 네트워크를 훈련시킨 다음 네트워크가 출력하는 확률 분포에서 단어를 샘플링하여 새로운 텍스트를 생성하는 데 사용하는 것이다. 예를 들어, 다음 코드를 사용하여 Taylor Swift 노래의 가사에 LSTM 네트워크를 훈련시킨 다음 이를 사용하여 새로운 가사를 생성할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

# Load the data
data = pd.read_csv("taylor_swift_lyrics.csv")
# Extract the lyrics
lyrics = data["lyric"].str.lower()
# Join the lyrics into one string
text = " ".join(lyrics)
# Create a vocabulary of characters
vocab = sorted(set(text))
# Create a mapping of characters to indices
char2idx = {char:idx for idx, char in enumerate(vocab)}
# Create a mapping of indices to characters
idx2char = np.array(vocab)
# Convert the text to a sequence of indices
text_as_int = np.array([char2idx[char] for char in text])
# Define the length of each input sequence
seq_length = 100
# Create training examples and targets
examples_per_epoch = len(text) // (seq_length + 1)
char_dataset = tf.data.Dataset.from_tensor_slices(text_as_int)
sequences = char_dataset.batch(seq_length + 1, drop_remainder=True)
def split_input_target(chunk):
    input_text = chunk[:-1]
    target_text = chunk[1:]
    return input_text, target_text
dataset = sequences.map(split_input_target)
# Define the batch size and the buffer size
batch_size = 64
buffer_size = 10000
# Shuffle and batch the dataset
dataset = dataset.shuffle(buffer_size).batch(batch_size, drop_remainder=True)
# Define the model
vocab_size = len(vocab)
embedding_dim = 256
rnn_units = 1024
model = keras.Sequential([
    layers.Embedding(vocab_size, embedding_dim, batch_input_shape=[batch_size, None]),
    layers.LSTM(rnn_units, return_sequences=True, stateful=True),
    layers.Dense(vocab_size)
])
# Define the loss function
def loss(labels, logits):
    return keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)
# Compile the model
model.compile(loss=loss, optimizer="adam")
# Define the number of epochs
epochs = 10
# Train the model
history = model.fit(dataset, epochs=epochs)
# Save the model
model.save("taylor_swift_model.h5")
# Load the model
model = keras.models.load_model("taylor_swift_model.h5")
# Define a function to generate text
def generate_text(model, start_string):
    # Define the number of characters to generate
    num_generate = 1000
    # Convert the start string to a sequence of indices
    input_eval = [char2idx[char] for char in start_string]
    input_eval = tf.expand_dims(input_eval, 0)
    # Create an empty list to store the generated text
    text_generated = []
    # Define the temperature to control the randomness of the predictions
    temperature = 1.0
    # Reset the states of the model
    model.reset_states()
    # Loop through the number of characters to generate
    for i in range(num_generate):
        # Predict the next character probabilities
        predictions = model(input_eval)
        predictions = tf.squeeze(predictions, 0)
        # Divide the predictions by the temperature
        predictions = predictions / temperature
        # Sample the next character index from the predictions
        predicted_id = tf.random.categorical(predictions, num_samples=1)[-1,0].numpy()
        # Add the predicted character to the input
        input_eval = tf.expand_dims([predicted_id], 0)
        # Add the predicted character to the generated text
        text_generated.append(idx2char[predicted_id])
    # Return the start string and the generated text
    return (start_string + "".join(text_generated))
# Generate new lyrics
print(generate_text(model, start_string="I love you"))
```

이 코드는 출력은 다음과 같을 수 있다.

```
I love you like a star in the sky
You're the only one who can make me feel alive
You're the reason why I'm breathing
You're the melody in my heart
You're the song that I'm singing

And I don't wanna lose you
I don't wanna let you go
You're the best thing that ever happened to me
You're the only one who knows me so well
You're the light in my darkness
You're the fire in my soul
You're the spark that keeps me going
And I can't get enough of you
I can't get enough of your love
You're the only one who can fill me up
You're the only one who can make me whole
You're the dream that I'm chasing
You're the magic in my life
You're the wonder that I'm feeling
And I don't wanna lose you
I don't wanna let you go
You're the best thing that ever happened to me
You're the only one who knows me so well
You're the light in my darkness
You're the fire in my soul
You're the spark that keeps me going
And I love you more than words can say
I love you more than you'll ever know
You're the only one who can make me stay
You're the only one who can make me grow
You're the story that I'm writing
You're the poetry in my mind
You're the beauty that I'm seeing
And I don't wanna lose you
I don't wanna let you go
You're the best thing that ever happened to me
You're the only one who knows me so well
You're the light in my darkness
You're the fire in my soul
You're the spark that keeps me going
```

보다시피 LSTM 네트워크는 'I love you'라는 입력 문자열을 기반으로 Taylor Swift 곡의 스타일과 내용이 유사한 새로운 가사를 생성할 수 있다. 물론 생성된 가사가 완벽한 것은 아니며, 일부 문법적 오류나 말도 안 되는 문구가 포함되어 있을 수 있지만 여전히 인상적이고 창의적이다.

LSTM의 또 다른 일반적인 어플리케이션은 기계 번역인데, 이는 한 언어에서 다른 언어로 텍스트를 번역하는 작업이다. 예를 들어, LSTM을 사용하여 영어 텍스트를 프랑스어 텍스트로 번역하거나 그 반대의 경우도 가능하다. 기본 아이디어는 입력된 텍스트를 벡터 표현으로 인코딩하는 LSTM 네트워크를 인코더로 사용하고, 벡터 표현을 출력된 텍스트로 디코딩하는 다른 LSTM 네트워크를 디코더로 사용하는 것이다. 예를 들어, 다음 코드를 사용하여 영어-프랑스어 문장 쌍에 대해 LSTM 인코더-디코더 네트워크를 훈련한 다음, 이를 사용하여 새로운 문장을 번역할 수 있다.

```python
# Define the model parameters
batch_size = 64  # Batch size for training.
epochs = 100  # Number of epochs to train for.
latent_dim = 256  # Latent dimensionality of the encoding space.
num_samples = 10000  # Number of samples to train on.
# Path to the data txt file on disk.
data_path = "eng-fra.txt"

# Vectorize the data
input_texts = []
target_texts = []
input_characters = set()
target_characters = set()
with open(data_path, "r", encoding="utf-8") as f:
    lines = f.read().split("\n")
for line in lines[: min(num_samples, len(lines) - 1)]:
    input_text, target_text, _ = line.split("\t")
    # We use "tab" as the "start sequence" character
    # for the targets, and "\n" as "end sequence" character.
    target_text = "\t" + target_text + "\n"
    input_texts.append(input_text)
    target_texts.append(target_text)
    for char in input_text:
        if char not in input_characters:
            input_characters.add(char)
    for char in target_text:
        if char not in target_characters:
            target_characters.add(char)

input_characters = sorted(list(input_characters))
target_characters = sorted(list(target_characters))
num_encoder_tokens = len(input_characters)
num_decoder_tokens = len(target_characters)
max_encoder_seq_length = max([len(txt) for txt in input_texts])
max_decoder_seq_length = max([len(txt) for txt in target_texts])

print("Number of samples:", len(input_texts))
print("Number of unique input tokens:", num_encoder_tokens)
print("Number of unique output tokens:", num_decoder_tokens)
print("Max sequence length for inputs:", max_encoder_seq_length)
print("Max sequence length for outputs:", max_decoder_seq_length)

input_token_index = dict([(char, i) for i, char in enumerate(input_characters)])
target_token_index = dict([(char, i) for i, char in enumerate(target_characters)])

encoder_input_data = np.zeros(
    (len(input_texts), max_encoder_seq_length, num_encoder_tokens), dtype="float32"
)
decoder_input_data = np.zeros(
    (len(input_texts), max_decoder_seq_length, num_decoder_tokens), dtype="float32"
)
decoder_target_data = np.zeros(
    (len(input_texts), max_decoder_seq_length, num_decoder_tokens), dtype="float32"
)

for i, (input_text, target_text) in enumerate(zip(input_texts, target_texts)):
    for t, char in enumerate(input_text):
        encoder_input_data[i, t, input_token_index[char]] = 1.0
    encoder_input_data[i, t + 1 :, input_token_index[" "]] = 1.0
    for t, char in enumerate(target_text):
        # decoder_target_data is ahead of decoder_input_data by one timestep
        decoder_input_data[i, t, target_token_index[char]] = 1.0
        if t > 0:
            # decoder_target_data will be ahead by one timestep
            # and will not include the start character.
            decoder_target_data[i, t - 1, target_token_index[char]] = 1.0
    decoder_input_data[i, t + 1 :, target_token_index[" "]] = 1.0
    decoder_target_data[i, t:, target_token_index[" "]] = 1.0

# Define the encoder and decoder layers
# Define an input sequence and process it.
encoder_inputs = keras.Input(shape=(None, num_encoder_tokens))
encoder = keras.layers.LSTM(latent_dim, return_state=True)
encoder_outputs, state_h, state_c = encoder(encoder_inputs)
# We discard `encoder_outputs` and only keep the states.
encoder_states = [state_h, state_c]

# Set up the decoder, using `encoder_states` as initial state.
decoder_inputs = keras.Input(shape=(None, num_decoder_tokens))
# We set up our decoder to return full output sequences,
# and to return internal states as well. We don't use the
# return states in the training model, but we will use them in inference.
decoder_lstm = keras.layers.LSTM(latent_dim, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
decoder_dense = keras.layers.Dense(num_decoder_tokens, activation="softmax")
decoder_outputs = decoder_dense(decoder_outputs)

# Build the training model
model = keras.Model([encoder_inputs, decoder_inputs], decoder_outputs)

# Compile and fit the model
model.compile(
    optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"]
)
model.fit(
    [encoder_input_data, decoder_input_data],
    decoder_target_data,
    batch_size=batch_size,
    epochs=epochs,
    validation_split=0.2,
)
# Save model
model.save("s2s")

# Define the inference model
# Restore the model and construct the encoder and decoder.
model = keras.models.load_model("s2s")

encoder_inputs = model.input[0]  # input_1
encoder_outputs, state_h_enc, state_c_enc = model.layers[2].output  # lstm_1
encoder_states = [state_h_enc, state_c_enc]
encoder_model = keras.Model(encoder_inputs, encoder_states)

decoder_inputs = model.input[1]  # input_2
decoder_state_input_h = keras.Input(shape=(latent_dim,))
decoder_state_input_c = keras.Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
decoder_lstm = model.layers[3]
decoder_outputs, state_h_dec, state_c_dec = decoder_lstm(
    decoder_inputs, initial_state=decoder_states_inputs
)
decoder_states = [state_h_dec, state_c_dec]
decoder_dense = model.layers[4]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = keras.Model(
    [decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states
)

# Reverse-lookup token index to decode sequences back to
# something readable.
reverse_input_char_index = dict((i, char) for char, i in input_token_index.items())
reverse_target_char_index = dict((i, char) for char, i in target_token_index.items())


def decode_sequence(input_seq):
    # Encode the input as state vectors.
    states_value = encoder_model.predict(input_seq)

    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1, 1, num_decoder_tokens))
    # Populate the first character of target sequence with the start character.
    target_seq[0, 0, target_token_index["\t"]] = 1.0

    # Sampling loop for a batch of sequences
    # (to simplify, here we assume a batch of size 1).
    stop_condition = False
    decoded_sentence = ""
    while not stop_condition:
        output_tokens, h, c = decoder_model.predict([target_seq] + states_value)

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_char = reverse_target_char_index[sampled_token_index]
        decoded_sentence += sampled_char

        # Exit condition: either hit max length
        # or find stop character.
        if sampled_char == "\n" or len(decoded_sentence) > max_decoder_seq_length:
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1, 1, num_decoder_tokens))
        target_seq[0, 0, sampled_token_index] = 1.0

        # Update states
        states_value = [h, c]

    return decoded_sentence
```

## <a name="summary"></a> 요약
이 포스팅에서는 순차적인 데이터를 처리하고 장기 의존성을 학습할 수 있는 순환 신경망의 일종인 장단기 기억 네트워크, 즉 LSTM에 대해 설명하였다. LSTM의 작동 방식과 단순 RNN에 영향을 미치는 기울기 소실 문제를 극복하는 방법에 대해 설명하였다. 텍스트 생성이나 기계 번역과 같이 LSTM을 다양한 작업과 도메인에 어떻게 적용할 수 있는지에 대한 예도 보였다. 또한 Keras 라이브러리를 사용하여 Python에서 LSTM을 구현하는 방법도 다루었다.

LSTM은 순차적 데이터를 처리하기 위한 강력하고 다재다능한 도구이며, 자연어 처리, 음성 인식, 컴퓨터 비전 등 많은 어플리케이션 분야를 가지고 있다. 그러나 LSTM이 장기 종속성을 처리할 수 있는 순환 신경망의 유일한 종류는 아니다. 게이트 순환 유닛(GRU), 양방향 RNN, 어텐션 메커니즘 및 트랜스포머와 같이 구조와 특성이 다른 RNN의 다른 변형과 확장이 있다. 이러한 고급 주제에 대해 더 알고 싶다면 다음 리소스를 확인할 수 있다.

- [Understanding GRU Networks](https://medium.com/@anishnama20/understanding-gated-recurrent-unit-gru-in-deep-learning-2e54923f3e2)
- [Bidirectional RNNs for Natural Language Processing](https://www.geeksforgeeks.org/bidirectional-rnns-in-nlp/)
- [Attention Is All You Need](https://papers.nips.cc/paper/7181-attention-is-all-you-need)
- [Illustrated Guide to Transformers](https://towardsdatascience.com/illustrated-guide-to-transformers-step-by-step-explanation-f74876522bc0)

